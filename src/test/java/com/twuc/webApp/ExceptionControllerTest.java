package com.twuc.webApp;

import org.apache.coyote.Response;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.portable.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ExceptionControllerTest {
    @Autowired
    private TestRestTemplate template;

    @Test
    void should_handle_exception() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/illegal-argument",String.class);
        assertEquals(500,entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument",entity.getBody());
    }

    @Test
    void should_handle_by_definite_handler() {
        ResponseEntity<String> entity = template.getForEntity("/api/exceptions/access-control-exception",String.class);
        assertEquals("Something wrong with the argument",entity.getBody());
        assertEquals(403,entity.getStatusCodeValue());
    }

    @Test
    void should_handle_double_exceptions_one() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/null-pointer",String.class);
        assertEquals("Something wrong with the argument",entity.getBody());
        assertEquals(418,entity.getStatusCodeValue());
    }

    @Test
    void should_handle_double_exceptions_two() {
        ResponseEntity<String> entity = template.getForEntity("/api/errors/arithmetic",String.class);
        assertEquals("Something wrong with the argument", entity.getBody());
        assertEquals(418, entity.getStatusCodeValue());
    }

    @Test
    void should_handle_sister_exceptions() {
        ResponseEntity<String> entity = template.getForEntity("/api/sister-errors/illegal-argument",String.class);
        assertEquals("Something wrong with brother or sister.", entity.getBody());
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("application/json",entity.getHeaders().getContentType().toString());
    }

    @Test
    void should_handle_brother_exceptions() {
        ResponseEntity<String> entity = template.getForEntity("/api/brother-errors/illegal-argument",String.class);
        assertEquals("Something wrong with brother or sister.", entity.getBody());
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("application/json",entity.getHeaders().getContentType().toString());
    }
}
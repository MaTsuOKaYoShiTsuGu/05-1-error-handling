package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class ExceptionController {

    @GetMapping("/api/errors/illegal-argument")
    Void getErrorAndHandle(){
        throw new RuntimeException();
    }

    @ExceptionHandler(RuntimeException.class)
    ResponseEntity handleException(RuntimeException exception){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Something wrong with the argument");
    }

    @GetMapping("/api/exceptions/access-control-exception")
    Void getException() {
        throw new AccessControlException("AccessControlException");
    }

    @ExceptionHandler(AccessControlException.class)
    ResponseEntity handleException(AccessControlException exception){
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body("Something wrong with the argument");
    }

    @GetMapping("/api/errors/null-pointer")
    void getNullPointerException(){
        throw new NullPointerException();
    }

    @GetMapping("/api/errors/arithmetic")
    void getArithmeticException(){
        throw new ArithmeticException();
    }

    @ExceptionHandler({NullPointerException.class,ArithmeticException.class})
    ResponseEntity getDoubleException(){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body("Something wrong with the argument");
    }

    @GetMapping("/api/sister-errors/illegal-argument")
    void getSisterException(){
        throw new IllegalArgumentException();
    }

    @GetMapping("/api/brother-errors/illegal-argument")
    void getBrotherException(){
        throw new IllegalArgumentException();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity getSisterAndBrotherException(){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .contentType(MediaType.APPLICATION_JSON)
                .body("Something wrong with brother or sister.");
    }
}
